// eslint-disable-next-line no-undef
module.exports = {
  mode: 'modules',
  out: 'public',
  exclude: ['**/node_modules/**', '**/*.spec.ts'],
  name: 'Sensio Network SDK for nodejs and javascript',
  readme: 'README.md',
}
