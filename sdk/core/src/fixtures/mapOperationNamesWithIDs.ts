import { SnGenericId } from '@sensio/types'

export interface OperationNameId {
  id: SnGenericId
  name: string
}
export const operationNamesWithIds: OperationNameId[] = [
  {
    id: 'bafy2bzaceanqw2xwjawh4zdrn6lct7pi63fndib5jhcx7hfe5y7g6iwwrwqka',
    name: '@sensio/op-sn-enc-hex',
  },
  {
    id: 'bafy2bzacealjmcwnfurdmn2tndsv35d5cdbaempopwdq5pr5zr56yrfhuc2zk',
    name: '@sensio/op-sn-split',
  },
  {
    id: 'bafy2bzacebc64bhgsj24ad53pkgxb7xi3zdedshiozl33nqplv7t5y27tii2u',
    name: '@sensio/op-sn-json-enc',
  },
  {
    id: 'bafy2bzaced7rpfi7k4atlkdaz5x6dmztvzm2jwz7on6lcykdswuf5s2jn4yo4',
    name: '@sensio/op-sn-json-dec',
  },
  {
    id: 'bafy2bzacecc2mmjx3nltb7pkdbtvjf64pjoswimxrcowguwi5jxn34blzb44s',
    name: '@sensio/op-sn-identity',
  },
  {
    id: 'bafy2bzacecxvve6bdbjvokspk2ocvk6m2yvrykcguzluf6q2criwnwzz4hnnw',
    name: '@sensio/op-sn-match-all',
  },
  {
    id: 'bafy2bzaceav6aryxb4qisakn64r4irptupvljhiv5l3xxiamlxiglrh2wyq2e',
    name: '@sensio/op-sn-match-none',
  },
  {
    id: 'bafy2bzacea24txwqzwanzte5laqhsy3umk4wq43h3llvlvkebp7gv73kzuzsi',
    name: '@sensio/op-sn-multihash',
  },
  {
    id: 'bafy2bzaced67meyosnnt6dmvsp4sjro5mo5ufu3ntoqamouttsa4tlqo6ta2m',
    name: '@sensio/op-sn-file',
  },
  {
    id: 'bafy2bzacecnosczzvuuibkajhvporgd4e5rrye4wcorvm7zmloinm6ofngdeq',
    name: '@sensio/op-sn-create-qrcode',
  },
  {
    id: 'bafy2bzacebr4fj6rnexne25v6zcuomsfnbfxrv22cwzrrqexeo3ggotd3u3zu',
    name: '@sensio/op-sn-create-ownership-claims',
  },
  {
    id: 'bafy2bzacecmqyut2oozadzigqsggrhkqxd2fxovubcy4djibfyci6iq6t4s4u',
    name: '@sensio/op-sn-save-statements',
  },
  {
    id: 'bafy2bzacebtqlbme62wogt4a7447lxsebvu5fncnht3eedmyvjle7t32ue46c',
    name: '@sensio/op-sn-user-sign-claims',
  },
  {
    id: 'bafy2bzacedtcdvuykc4jencdnm5zymadzutm7yzvpughbgjotxmgpyglzhqpc',
    name: '@sensio/op-sn-take-photo-and-upload-qrcode',
  },
  {
    id: 'bafy2bzacebvd3b7upai2av3w33dwrwqkh7w7qvbon6obi6q4evw6pqe744npu',
    name: '@sensio/op-sn-cid',
  },
  {
    id: 'bafy2bzaceczzm4kzeqfqnk4onhpnipiskfqcxtssm7t54dmrzso5un6eaukya',
    name: '@sensio/op-sn-image-metadata-hash',
  },
  {
    id: 'bafy2bzacedk46pihe5gr44l6liofvga6umxyhy27hwp4treiq32y6cdywsxzm',
    name: '@sensio/op-sn-image-metadata',
  },
  {
    id: 'bafy2bzaceafhfdbh4r6fmmnudwqehqedhf632dbafl7fnmbkbpjnv2miacdza',
    name: '@sensio/op-sn-image-phash',
  },
  {
    id: 'bafy2bzacecgmp4enyoywse6enuoqgfpffojhrn7kovryv775xa7mhv6f34qju',
    name: '@sensio/op-sn-image-raw-pixels-hash',
  },
  {
    id: 'bafy2bzacecjrerw6cnuyj3ta7ntsvleq43vxgt4otgdvf2wjjpdg5almrzd4y',
    name: '@sensio/op-sn-image-raw-pixels',
  },
]
