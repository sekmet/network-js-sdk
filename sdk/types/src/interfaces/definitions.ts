/**
 * DO NOT DELETE!
 * THIS file must be maintained for the new pallets and new types  for the old pallets
 */

export { default as operations } from './operations/definitions'
export { default as poe } from './poe/definitions'
export { default as rules } from './rules/definitions'
export { default as sensio } from './sensio/definitions'
export { default as statements } from './statements/definitions'
