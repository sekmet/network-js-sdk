# @sensio/op-sn-json-dec

Blockchain Version **bafy2bzaced7rpfi7k4atlkdaz5x6dmztvzm2jwz7on6lcykdswuf5s2jn4yo4**

## Description

Wrapper of JSON.parse()

## Links and Repo

- npm: https://www.npmjs.com/package/@sensio/op-sn-json-dec
- repo: [repo folder](https://gitlab.com/sensio_group/network-js-sdk/-/tree/master/operations/snJsonDec)
- support: [Discord server #public-support channel](https://discord.gg/RQ9g29y)

## Install

```sh
# install latest version
yarn add @sensio/op-sn-json-dec

# or specific version
yarn add @sensio/op-sn-json-dec@0.3.1
```

## Usage

```ts
import snJsonDec from '@sensio/op-sn-json-dec'

const data = new U8intArray(7)
await snJsonDec(data)
```

## Contributing

PRs accepted.

## License

Longer version is in LICENSE file

Apache-2.0 © [Sensio Group](https://sensio.group)
