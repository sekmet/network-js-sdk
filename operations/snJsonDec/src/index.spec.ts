import snJsonDec, { config } from '.'

describe('SnOperation: snJsonDec', (): void => {
  it('is snJsonDec defined', (): void => {
    expect(snJsonDec).toBeDefined()
  })
  it('is config defined', (): void => {
    expect(config).toBeDefined()
  })
})
