import snSplit, { config } from '.'

describe('SnOperation: snSplit', (): void => {
  it('is snSplit defined', (): void => {
    expect(snSplit).toBeDefined()
  })
  it('is config defined', (): void => {
    expect(config).toBeDefined()
  })
})
