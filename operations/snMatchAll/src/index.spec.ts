import snMatchAll, { config } from '.'

describe('SnOperation: snMatchAll', (): void => {
  it('is snMatchAll defined', (): void => {
    expect(snMatchAll).toBeDefined()
  })
  it('is config defined', (): void => {
    expect(config).toBeDefined()
  })
})
