export { default as config } from './config'
export * from './interfaces'
export * from './module'
import snUserSignClaims from './module'

export default snUserSignClaims
