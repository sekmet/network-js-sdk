# @sensio/op-sn-user-sign-claims

Blockchain Version **bafy2bzacebtqlbme62wogt4a7447lxsebvu5fncnht3eedmyvjle7t32ue46c**

## Description

Sign the claims and return the tuple of claims and their signatures

## Links and Repo

- npm: https://www.npmjs.com/package/@sensio/op-sn-user-sign-claims
- repo: [repo folder](https://gitlab.com/sensio_group/network-js-sdk/-/tree/master/operations/snUserSignClaims)
- support: [Discord server #public-support channel](https://discord.gg/RQ9g29y)

## Install

```sh
# install latest version
yarn add @sensio/op-sn-user-sign-claims

# or specific version
yarn add @sensio/op-sn-user-sign-claims@0.1.0
```

## Usage

For the usage look at the test [index.spec.ts](./src/index.spec.ts)

## Contributing

PRs accepted.

## License

Longer version is in LICENSE file

Apache-2.0 © [Sensio Group](https://sensio.group)
