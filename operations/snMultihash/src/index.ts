export { default as config } from './config'
export * from './interfaces'
export * from './module'
import snMultihash from './module'

export default snMultihash
