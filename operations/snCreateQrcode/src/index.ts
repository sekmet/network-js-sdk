export { default as config } from './config'
export * from './interfaces'
export * from './module'
import snCreateQrcode from './module'

export default snCreateQrcode
