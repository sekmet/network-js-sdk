export { default as config } from './config'
export * from './interfaces'
export * from './module'
import snIdentity from './module'

export default snIdentity
