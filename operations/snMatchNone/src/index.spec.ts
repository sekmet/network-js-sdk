import snMatchNone, { config } from '.'

describe('SnOperation: snMatchNone', (): void => {
  it('is snMatchNone defined', (): void => {
    expect(snMatchNone).toBeDefined()
  })
  it('is config defined', (): void => {
    expect(config).toBeDefined()
  })
})
