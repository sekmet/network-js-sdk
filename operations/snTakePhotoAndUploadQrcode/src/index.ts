export { default as config } from './config'
export * from './interfaces'
export * from './module'
import snTakePhotoAndUploadQrcode from './module'

export default snTakePhotoAndUploadQrcode
