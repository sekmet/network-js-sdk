import snJsonEnc, { config } from '.'

describe('SnOperation: snJsonEnc', (): void => {
  it('is snJsonEnc defined', (): void => {
    expect(snJsonEnc).toBeDefined()
  })
  it('is config defined', (): void => {
    expect(config).toBeDefined()
  })
})
