export { default as config } from './config'
export * from './interfaces'
export * from './module'
import snCreateOwnershipClaims from './module'

export default snCreateOwnershipClaims
